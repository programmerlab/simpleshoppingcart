@extends('layouts.app') 
@section('content') 
    <div class="container" ng-app="loginApp" ng-controller="loginController">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal"   name="userForm" ng-submit="submitLoginForm()">
                        {{ csrf_field() }}

                     

                         <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label"></label>

                            <div class="col-md-6">
                                   <div ng-model="invalidCredetial" class="alert @{{alert_danger}}">@{{invalidCredetial}}</div>
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" ng-model="email" >
                                  <span class="help-block" ng-show="errorEmail" style="color:red">@{{errorEmail}}</span>
                             
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" ng-model="password" >
                                   <span class="help-block" ng-show="errorPassword" style="color:red">@{{errorPassword}}</span>
                               
                            </div>
                        </div>
 

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/register') }}">Don't have account !</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
