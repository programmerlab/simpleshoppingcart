<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\ProductRequest;
use Modules\Admin\Models\User;
use Modules\Admin\Models\Category;
use Modules\Admin\Models\Product;
use Input;
use Validator;
use Auth;
use Paginate;
use Grids;
use HTML;
use Form;
use Hash;
use View;
use URL;
use Lang;
use Session;
use Route;
use Crypt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Dispatcher; 
use App\Helpers\Helper;
use Response;
use Cart;

/**
 * Class AdminController
 */
class ProductController extends Controller {
    /**
     * @var  Repository
     */
    private $user;
    /**
     * Displays all admin.
     *
     * @return \Illuminate\View\View
     */
    public function __construct(Request $request) {

        if ($request->session()->has('current_user')) {
            View::share('user', $request->session()->get('current_user'));
            $this->user =  $request->session()->get('current_user');
        }else{
             View::share('user', null);
               $this->user = null;
        }
    } 

    /*
     * Home page
     * */

    public function index(Request $request) 
    {  
        $cart = Cart::content();   
        $pid = [];
        foreach ($cart as $key => $value) {
            $pid[] = $value->id;
        }
        $product_photo =   Product::whereIn('id',$pid)->get(['photo','id'])->toArray();
         
        return view('cart', compact('cart','product_photo'));
    }
    public function addToCart(Request $request, $id) 
    { 
           
        if ($request->isMethod('get')) {
            $product_id = $request->get('id');
            $product = Product::find($id);   
            Cart::add(array('id' => $product->id, 'name' => $product->product_title, 'qty' => 1, 'price' => $product->price,'photo'=>$product->photo));
        }
        
        $products = $product->all(); 
        $cart = Cart::content(); 
    
         return response()->json([
                'status' => 1,
                'message' => 'success',
                'cart'  => count($cart),
                'data'  =>  $products
                ]
            ); 
    }
    /* update Cart */
    public function updateCart(Request $request) 
    { 
        if ($request->get('product_id') && ($request->get('increment')) == 1) {
           
            $rowId = Cart::search(function($key, $value) use($request)
                        { 
                            return $key->id == $request->get('product_id'); 
                        }
                    );
            foreach ($rowId as $key => $value) {
                $rowId = $value->rowId; 
            }
              $item = Cart::get($rowId);
              $qty = intval($item->qty)+1;
              Cart::update($rowId,$qty);
            return Redirect::to('cart');
        }
        elseif ($request->get('product_id') && ($request->get('decrease')) == 1) {  
           $rowId = Cart::search(function($key, $value) use($request)
                        { 
                            return $key->id == $request->get('product_id'); 
                        }
                    );
            $total_qty = 0;
            foreach ($rowId as $key => $value) {
                $rowId = $value->rowId;
                $total_qty = $value->qty-1;
            }
            Cart::update($rowId, intval($total_qty));
            return Redirect::to('cart');
        }
    }
    /* Remove product from cart */
    public function clearCart(Request $request, Cart $cart)
    {
        $cart = Cart::content(); 
        foreach ($cart as $key => $value) {
             Cart::remove($key);
        }

        return Redirect::to('product');
    }
    /* Show landind page*/
    public function showProduct(Request $request, Product $product)
    {   
       $products = $product->all();     
        return view('welcome',compact('products')); 
    }
    /* Get product details */
    public function getProduct(Request $request, Product $product)
    {
        $products = $product->all(); 
        $cart = Cart::content(); 
    
         return json_encode(array(
                'status' => 1,
                'message' => 'success',
                'cart'  => count($cart),
                'data'  =>  $products
                )
            ); 
    } 
    /* Remove Item form cart */
    public function removeItem($id)
    {
        $rowId = Cart::search(function($key, $value) use($id)
            { 
                return $key->id == $id; 
            }
        );
        foreach ($rowId as $key => $value) {
            $rowId = $value->rowId; 
        }
        $item = Cart::get($rowId); 
        Cart::remove($rowId);
        return Redirect::to('cart'); 
    }
    /* product checkout */
    public function checkout()
    { 
        
        if(isset($this->user) && $this->user->count())
        {
            $cart = Cart::content(); 
            foreach ($cart as $key => $value) {
             Cart::remove($key);
            }
             return view('thankyou');
        }else{
             return view('auth.login');
        } 
    }
}
