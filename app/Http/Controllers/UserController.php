<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Input;
use Validator;
use Auth;
use Form;
use Hash;
use View;
use URL;
use Route;
use Crypt;
use Response;
use App\User;
use JWTAuth;
use Session; 
use Cart;

class UserController extends Controller
{

   	public function __construct(Request $request) {
        if ($request->session()->has('users')) {
            View::share('user', $request->session()->get('current_user'));
        }else{
             View::share('user', null);
        }
    }  
    public function index(Request $request)
    {  		
    	 return view('welcome');
    }
    /* @method : login
    * @param : email,password 
    * Response : json 
    * Author : kundan Roy   
    */
    public function login(Request $request)
    {    
        $input['email']         = $request->input('email'); 
        $input['password']      = Hash::make($request->input('password'));
        

        //Server side valiation
        $validator = Validator::make($request->all(), [
            'email'     =>  'required|email',
            'password' => 'required'
        ]);
        /** Return Error Message **/
        if ($validator->fails()) {
                    $error_msg  =   [];
            foreach ( $validator->messages()->messages() as $key => $value) {
                          $error_msg[$key] = $value[0];
                    }
                            
            return Response::json(array(
                'status' => 0,
                 "code" => 500,
                'cart' => 0,
                'message' => $error_msg,
                'data'  =>  ''
                )
            );
        }   
        
          $credentials = ['email' => Input::get('email'), 'password' => Input::get('password')];  
       
          if (Auth::attempt($credentials)) {
             $request->session()->put('current_user',Auth::user());
                  $cart = Cart::content(); 

                 return response()->json(
                            [ 
                                "status"=>1,
                                "code" => 200,
                                'cart' => count($cart),
                                "message"=>"Successfully logged in",
                                'data'=> $request->session()->get('current_user')
                            ]
                        );
          }else{  
                return response()->json(
                            [ 
                                "status"=>0,
                                "code" => 401, 
                                 'cart' => 0,
                                "message"=>"Invalid credentials",
                                'data'=> []
                            ]
                        );
          }  
    } 
 /* @method : signup
    * @param : first_name,email ,password
    * Response : json 
    * Author : kundan Roy   
    */
    public function signup(Request $request,User $user)
    {   
		$input['first_name'] 	= $request->input('name');
    	$input['email'] 		= $request->input('email'); 
    	$input['password'] 	    = Hash::make($request->input('password'));
    	

        //Server side valiation
        $validator = Validator::make($request->all(), [
           	'name'		=> 	'required',
            'email'     =>  'required|email|unique:users',
	        'password' => 'required|min:6',
	        'confirm_password' => 'required|same:password'
        ]);
        /** Return Error Message **/
        if ($validator->fails()) {
                   	$error_msg	=	[];
	        foreach ( $validator->messages()->messages() as $key => $value) {
	        			  $error_msg[$key] = $value[0];
	        		}
	        		 		
          	return Response::json(array(
	          	'status' => 0,
	            'message' => $error_msg,
	            'data'	=>	''
	            )
          	);
        }   
        /** --Create USER-- **/
        $user = User::create($input);  
        return response()->json(
                            [ 
                                "status"=>1,
                                "code" => 200,
                                "message"=>"Thank you for registration.",
                                'data'=>$request->except('password')
                            ]
                        );
    }
     /*
    * Show registration form
    */
    public function register(Request $request)
    {  
        return view('auth.register');
    }
    /*
    * Show login form
    */
    public function showLoginForm(Request $request)
    {  
        return view('auth.login');
    }
    /*
    * logout
    */
    public function signOut(Request $request)
    {
         $cart = Cart::content(); 
        foreach ($cart as $key => $value) {
             Cart::remove($key);
        }
        $request->session()->flush();
         return Redirect::to('product'); 
    }
}